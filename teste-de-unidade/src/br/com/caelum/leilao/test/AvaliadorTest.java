package br.com.caelum.leilao.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.com.caelum.leilao.builder.CriadorDeLeilao;
import br.com.caelum.leilao.dominio.Lance;
import br.com.caelum.leilao.dominio.Leilao;
import br.com.caelum.leilao.dominio.Usuario;
import br.com.caelum.leilao.servico.Avaliador;
import junit.framework.AssertionFailedError;



public class AvaliadorTest {
	
//	@BeforeClass
//	public static void testandoBeforeClass() {
//	  System.out.println("before class");
//	}
//
//	@AfterClass
//	public static void testandoAfterClass() {
//	  System.out.println("after class");
//	}
	
	private Avaliador leiloeiro;
	private Usuario joao, maria, jose, ana;

	@Before
	public void setUp() { //Nome anterior: criaAvaliador()
		
		this.leiloeiro = new Avaliador();
	}
	
	@Before
	public void criaUsuario() {
		
		this.joao = new Usuario("João");
		this.maria = new Usuario("Maria");
		this.jose = new Usuario("Jose");
		this.ana = new Usuario("Ana");
	}
	
	@Test(expected = RuntimeException.class)
	public void naoDeveAvaliarLeiloesSemNenhumLanceDado() {
		
		Leilao leilao = new CriadorDeLeilao().para("PlayStation 5 Novo")
				.constroiLance();
		
		leiloeiro.avalia(leilao);
		Assert.fail();
	}
	
	@Test
	public void main() {
		
		Leilao leilao 	= new Leilao("Playstation 5");
		
		leilao.propoe(new Lance(joao, 1000.0));
		leilao.propoe(new Lance(jose, 2000.0));
		leilao.propoe(new Lance(maria, 3000.0));
		
		leiloeiro.avalia(leilao);
		
		double maiorEsperado = 3000.0;
		double menorEsperado = 1000.0;		
		
		assertEquals(maiorEsperado, leiloeiro.getMaiorLance(), 0.00001);
		assertEquals(menorEsperado, leiloeiro.getMenorLance(), 0.00001);
	}
	
	@Test
	public void deveEntenderLeilaoComApenasUmLance() {
		
		Leilao leilao = new Leilao("Playstation 5");
		
		leilao.propoe(new Lance(joao, 1000.0));
		
		leiloeiro.avalia(leilao);
		
		assertEquals(1000.0, leiloeiro.getMaiorLance(), 0.00001);		
	}
	
	@Test
	public void deveEncontrarOsTresMaioresLances() {
		
		Leilao leilao = new CriadorDeLeilao().para("PS 5 Novo")
				.lance(joao, 100.0)
				.lance(maria, 200.0)
				.lance(jose, 300.0)
				.lance(ana, 400.0)
				.constroiLance();
		
		leiloeiro.avalia(leilao);
		
		List<Lance> maiores = leiloeiro.getTresMaioresLances();
		assertEquals(3, maiores.size());
		assertEquals(400.0, maiores.get(0).getValor(), 0.00001);
		assertEquals(300.0, maiores.get(1).getValor(), 0.00001);
		assertEquals(200.0, maiores.get(2).getValor(), 0.00001);
	}
	
//	@After
//	public void finaliza() {
//	  System.out.println("fim dos testes");
//	}
}
